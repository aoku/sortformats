# |/usr/bin/env python3

"""Ordenar una lista de formatos según su nivel de compresión"""

import sys

# Lista ordenada de formatos de imagen, de menor a mayor compresión
# (clasificación asumida)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Averiguar si format1 es menor que format2
    Devuelve True si formato1 es menor, False en caso contrario.
    Un formato es menor que otro si aparece antes en la lista fordered.
    """
    formato1 = fordered.index(format1)
    formato2 = fordered.index(format2)
    if formato1 >= formato2:
        return False
    else:
        return True


def find_lower_pos(formats: list, pivot: int) -> int:
    """Encontrar el formato más bajo en formatos después de pivote
    Devuelve el índice del formato más bajo encontrado"""

    format1 = pivot
    for format2 in range(pivot+1, len(formats)):
        if formats[format1] != formats[format2]:
            var = lower_than(formats[format1], formats[format2])
            if not var:
                format1 = format2

    return format1


def sort_formats(formats: list) -> list:
    """Ordenar la lista de formatos
    Devuelve la lista ordenada de formatos, de acuerdo a su
    posición en fordered"""
    sum1 = 0
    pivot = formats.index(formats[0])
    while True:
        sum1 += 1
        num = find_lower_pos(formats, pivot)
        pivote = formats[pivot]
        mas_bajo = formats[num]
        if sum1 != len(formats):
            var = lower_than(pivote, mas_bajo)
            if not var:
                formats[pivot], formats[num] = formats[num], formats[pivot]
                pivot += 1
        else:
            break

    return formats


def main():
    """Leer argumentos de línea de comandos, e imprimirlos ordenados
    También, comprobar si son formatos válidos usando la tupla fordered"""
    sum2 = 0
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        sum2 += 1
        if sum2 == len(sorted_formats):
            print(format)
        else:
            print(format, end=" ")


if __name__ == '__main__':
    main()
